/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

struct Articulo{
    int codigoProducto;
    float precio;
    int existencia;
    char nombre[20];
};

//arreglo de elementos tipo Articulo
struct Articulo articulos[]={{1,112.50, 10, "procesador"},{2,112.50, 10, "disipador"}, {3,112.50, 10, "gabinete"}, {4,112.50, 10, "motherboard"}, {5,112.50, 10, "monitor"}};

//obtiene el tamaño del arreglo articulos
int n = sizeof(articulos) / sizeof(articulos[0]);

//imprime todos los elementos del arreglo
void imprimirArray(){
    for(int i = 0; i<n; i++){
        printf(" codigoProducto: %i  precio: %f nombre: %s  existencia: %i \n",articulos[i].codigoProducto,articulos[i].precio, articulos[i].nombre ,articulos[i].existencia); 
    }
};

//agrega un elemento de tipo Articulo al array articulos
void agregarArticulo(){
    int codigo, existencia;
    float precio;
    char nombre [20];
    
    struct Articulo nuevoArticulo;
    
    printf("escribe el codigo del Articulo: ");
    scanf("%d",&nuevoArticulo.codigoProducto);
    
    printf("escribe el precio del Articulo: ");
    scanf("%f",&nuevoArticulo.precio);
    
    printf("escribe el stock del Articulo: ");
    scanf("%d",&nuevoArticulo.existencia);
    
    printf("escribe el nombre del Articulo (max 20 caracteres): ");
    scanf("%s",nuevoArticulo.nombre);
    
    printf("codigo: %i precio: %f existencia: %i nombre: %s \n", nuevoArticulo.codigoProducto, nuevoArticulo.precio, nuevoArticulo.existencia, nuevoArticulo.nombre );
    
    //TODO: ingresar este articulo al arreglo articulos
};

//cambia el orden de articulos por nombre
void ordenarArrayNombre(){
    
};

//cambia el orden de articulos por precio
void ordenarArrayPrecio(){
    
};

//suma todos los precios de los articulos multiplicado por la existencia de cada uno
float costoTotalArray(){
    float costoTotal = 0.0;
    for(int i = 0; i<n; i++){
        costoTotal+=(articulos[i].precio*articulos[i].existencia);
    }
    return costoTotal;
};

//muestra que nombres cuya inicial se encuentra entre la primerletra y la ultimaletra
void obtenerNombres(char primerletra, char ultimaletra){
    printf("\nRango: [%c - %c]\n",primerletra, ultimaletra);
    int contador = 0;
    for(int i = 0; i<n; i++){
        if(articulos[i].nombre[0]>=primerletra && articulos[i].nombre[0]<= ultimaletra){
            contador++;
            printf("nombre en rango: %s \n",articulos[i].nombre);
        }

    }
    printf("total de nombres en rango: %i",contador);
};

//guarda articulos en un archivo de texto plano
void almacenarElementos(){};

//abre un archivo de texto plano y luego lo lee para crear un array articulos con esa info
void leerElementos(){};

//guarda articulos en un archivo binario
void almacenarElementosBinario(){};

//abre un archivo binario y luego lo lee para crear un array articulos con esa info
void leerElementosBinario(){};


int main()
{

    imprimirArray(); //pasa
    //obtenerNombres('l','q'); //pasa
    //printf("costoTotal: %f \n",costoTotalArray()); //pasa
    //agregarArticulo(); //pasa
    
    return 0;
}
